package com.fedco.mbc.model;

public class BaiduLocationModel {

    double lat;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    double lon;

    static BaiduLocationModel baiduLocationModel;


    public static BaiduLocationModel getBaiduModelInstarnce() {
        if (baiduLocationModel != null) {
            return baiduLocationModel;
        }
        else {

            return new BaiduLocationModel ();
        }

    }


}
