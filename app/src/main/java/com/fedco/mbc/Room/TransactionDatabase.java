package com.fedco.mbc.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database ( entities = TransactionDetailsTable.class,version = 1,exportSchema = false)
public abstract class TransactionDatabase extends RoomDatabase {

    public abstract TransactionDao transactionDao();


    private static final String DB_NAME = "TransactionDetails.db";
    private static volatile TransactionDatabase instance;

    public static synchronized TransactionDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static TransactionDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                TransactionDatabase.class,
                DB_NAME).allowMainThreadQueries ().build();
    }

}
