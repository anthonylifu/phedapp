package com.fedco.mbc.Room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.fedco.mbc.CustomClasses.TransactionDetailsModel;

import java.util.List;

@Dao
public interface TransactionDao {


    @Insert
    void insertTransactionDetails(TransactionDetailsTable transactionDetails);


    @Query("select * from TransactionTable ")
    List <TransactionDetailsTable> getAllTransactionDetails();

    @Delete
    void deleteTransaction(TransactionDetailsTable transactionDetailsTable);
}
